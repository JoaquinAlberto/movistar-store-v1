package com.movistar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemovistarStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemovistarStoreApplication.class, args);
	}

}
