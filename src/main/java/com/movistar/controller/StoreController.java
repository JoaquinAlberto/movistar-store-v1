package com.movistar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.movistar.model.Store;
import com.movistar.service.IStoreService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

@Api(value = "StoreController", tags = { "Store Controller" })
@SwaggerDefinition(tags = { @Tag(name = "Store Controller", description = "api consulta y registro de tienda") })
@RestController
@RequestMapping("/v1/movistar-store")
public class StoreController {

	@Autowired
	private IStoreService storeService;

	@ApiOperation(value = "Obtener listado de tiendas", response = Store.class, tags = "listStore")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "404 error") })
	@GetMapping(value = "/")
	public ResponseEntity<List<Store>> listStore() {
		
		return ResponseEntity.ok(storeService.listTiendas());
		
	}

	@ApiOperation(value = "Registrar tiendas", tags = "registerStore")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 201, message = "CREATED"), @ApiResponse(code = 404, message = "404 error") })
	@PostMapping(value = "/")
	public ResponseEntity<Void> registrarTiendas(@RequestBody List<Store> store) {

		if (store == null || store.size() == 0) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			storeService.registrarTiendas(store);
			return new ResponseEntity<>(HttpStatus.CREATED);
		}

	}

}
