package com.movistar.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movistar.model.Store;

@Repository
public interface IStoreRepository extends JpaRepository<Store, String> {

}
