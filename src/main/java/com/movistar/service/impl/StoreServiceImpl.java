package com.movistar.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.movistar.model.Store;
import com.movistar.repository.IStoreRepository;
import com.movistar.service.IStoreService;

@Service
public class StoreServiceImpl implements IStoreService{
	
	@Autowired
	private IStoreRepository storeRepository;

	@Override
	public List<Store> listTiendas() {
	
		return storeRepository.findAll();
		
	}

	@Override
	public void registrarTiendas(List<Store> obj) {

		storeRepository.save(obj);
		
	}

	

}
