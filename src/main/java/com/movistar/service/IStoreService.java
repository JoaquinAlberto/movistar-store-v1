package com.movistar.service;

import java.util.List;

import com.movistar.model.Store;

public interface IStoreService {

	List<Store> listTiendas();
	
	void registrarTiendas(List<Store> obj);
	
	
}
