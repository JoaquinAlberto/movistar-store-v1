package com.movistar.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "Store")
@ApiModel("Store")
public class Store {

	@Id
	@Column(name = "store_code", length = 255, nullable = false)
	@ApiModelProperty(value = "Codigo de tienda")
	String store_code;
	@Column(name = "store_name", length = 255, nullable = false)
	@ApiModelProperty(value = "Nombre de tienda")
	String store_name;
	@Column(name = "store_creation", length = 255, nullable = false)
	@ApiModelProperty(value = "Fecha de registro de tienda")
	String store_creation;
	
	
	public String getStore_code() {
		return store_code;
	}
	public void setStore_code(String store_code) {
		this.store_code = store_code;
	}
	public String getStore_name() {
		return store_name;
	}
	public void setStore_name(String store_name) {
		this.store_name = store_name;
	}
	public String getStore_creation() {
		return store_creation;
	}
	public void setStore_creation(String store_creation) {
		this.store_creation = store_creation;
	}
	
	
	
	
}
