package com.movistar.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class StoreControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void listStore() throws Exception {
        this.mvc.perform(get("/v1/movistar-store/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json; charset=utf-8"));
    }

    @Test
    public void registrarTiendas() throws Exception{
        this.mvc.perform(post("/v1/movistar-store/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[  {\n" +
                        "        \"store_code\": \"R3\",\n" +
                        "        \"store_name\": \"R3D\",\n" +
                        "        \"store_creation\": \"23/04/2020\"\n" +
                        "    }]")).andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void registrarTiendasError() throws Exception{
        this.mvc.perform(post("/v1/movistar-store/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("[]")).andExpect(status().isBadRequest())
                .andReturn();
    }
}